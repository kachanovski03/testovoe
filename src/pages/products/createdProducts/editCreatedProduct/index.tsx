import React, { useEffect } from "react";
import { EditCreatedProduct } from "./editCreatedProduct";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootStateType } from "../../../../redux/store";
import { ICreatedProduct } from "../../../../interfaces";
import {
  editCreatedProduct,
  getCreatedProductById,
  removeCreatedProduct,
} from "../../../../redux/thunks";
import { ICommonReducer } from "../../../../redux/reducers";
import { LOCALSTORAGE_KEYS } from "../../../../constants";
import { createdProductsActions } from "../../../../redux/actions";

export const Container = () => {
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const product = useSelector<RootStateType, ICreatedProduct>(
    (state) => state.createdProducts.createdProduct
  );
  const { isFetching } = useSelector<RootStateType, ICommonReducer>(
    (state) => state.common
  );
  useEffect(() => {
    dispatch(
      getCreatedProductById(LOCALSTORAGE_KEYS.CREATED_PRODUCTS, Number(id))
    );
    if (!product) {
      navigate("/", { replace: true });
    }
    return () => {
      dispatch(createdProductsActions.setCreatedProduct({} as ICreatedProduct));
    };
  }, [dispatch, id]);

  const editProductHandler = (data: ICreatedProduct) => {
    dispatch(
      editCreatedProduct(LOCALSTORAGE_KEYS.CREATED_PRODUCTS, Number(id), data)
    );
  };

  const removeCreatedProductHandler = () => {
    dispatch(
      removeCreatedProduct(LOCALSTORAGE_KEYS.CREATED_PRODUCTS, Number(id))
    );
    navigate("/products");
  };

  return (
    <EditCreatedProduct
      removeCreatedProductHandler={removeCreatedProductHandler}
      isFetching={isFetching}
      editProductHandler={editProductHandler}
      product={product}
    />
  );
};
