import React, { useEffect, useState } from "react";
import { ServerProducts } from "./serverProducts";
import { useDispatch, useSelector } from "react-redux";
import { getProducts, removeProduct } from "../../../redux/thunks";
import { RootStateType } from "../../../redux/store";
import { IProduct } from "../../../interfaces";
import { productsActions } from "../../../redux/actions";
import { SORT_TYPE } from "../../../constants";
import { useNavigate } from "react-router-dom";
import { Loader } from "../../../components";

export const Container = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const products = useSelector<RootStateType, IProduct[]>(
    (state) => state.products.products
  );
  const isFetching = useSelector<RootStateType, boolean>(
    (state) => state.common.isFetching
  );

  const [limitCount, setLimitCount] = useState<number>(8);
  const [sortType, setSortType] = useState<SORT_TYPE>(SORT_TYPE.ASC);
  const [searchValue, setSearchValue] = useState<string>("");

  useEffect(() => {
    dispatch(getProducts());
    return () => {
      dispatch(productsActions.setProducts([]));
    };
  }, [dispatch]);

  const getProductsWithLimit = (count: string | string[]) => {
    setLimitCount(Number(count));
    dispatch(getProducts(Number(count), sortType));
  };

  const getProductsWithSort = (sort: string | string[]) => {
    setSortType(sort as SORT_TYPE);
    dispatch(getProducts(limitCount, sort as SORT_TYPE));
  };

  const removeItemHandler = (id: number) => {
    dispatch(removeProduct(id));
  };

  const editItemHandler = (id: number) => {
    navigate(`/product/edit/${id}`);
  };

  const onPressCard = (id: number) => {
    navigate(`/product/${id}`);
  };

  if (isFetching) {
    return <Loader />;
  }

  return (
    <ServerProducts
      searchValue={searchValue}
      setSearchValue={setSearchValue}
      getProductsWithSort={getProductsWithSort}
      getProductsWithLimit={getProductsWithLimit}
      removeItem={removeItemHandler}
      editItemHandler={editItemHandler}
      onPressCard={onPressCard}
      products={products}
    />
  );
};
