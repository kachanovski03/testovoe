export const localStorageUtils = {
  saveItem(key: string, item: any) {
    localStorage.setItem(key, JSON.stringify(item));
  },
  saveItems(key: string, item: any) {
    const items = localStorage.getItem(key);
    items
      ? localStorage.setItem(key, JSON.stringify([...JSON.parse(items), item]))
      : localStorage.setItem(key, JSON.stringify([item]));
  },
  getItem(key: string) {
    const item = localStorage.getItem(key);
    return item ? JSON.parse(item) : null;
  },
  removeItem(key: string) {
    localStorage.removeItem(key);
  },
};
