import React from "react";
import { IProduct } from "../../../../interfaces";
import { Grid, Text } from "@geist-ui/react";
import { ServerProductForm } from "../serverProductForm";

interface IProps {
  product: IProduct;
  editProductHandler: (data: IProduct) => void;
  isFetching: boolean;
  onRemoveHandler: () => void;
}

export const EditServerProduct = ({
  product,
  onRemoveHandler,
  editProductHandler,
  isFetching,
}: IProps) => {
  return (
    <Grid.Container justify={"center"}>
      <Grid xs={13} direction={"column"}>
        <Text h1>Редактировать товар</Text>
        <ServerProductForm
          onRemoveHandler={onRemoveHandler}
          product={product}
          editProductHandler={editProductHandler}
          isFetching={isFetching}
        />
      </Grid>
    </Grid.Container>
  );
};
