import React, { useEffect, useState } from "react";
import { commonActions } from "../../../redux/actions";
import { useDispatch } from "react-redux";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Button, Description, Grid, Input, Spacer } from "@geist-ui/react";
import {
  BUTTON_TYPE,
  FORM_MODE,
  INPUT_TYPES,
  TOAST_TYPES,
} from "../../../constants";
import { ICreatedProduct } from "../../../interfaces";

const schema = yup.object().shape({
  title: yup.string().required("Поле название обязательное"),
  description: yup.string().required("Поле описание обязательное"),
  price: yup
    .number()
    .required("Поле цена обязательное и должно содержать только цифры"),
});

interface IProps {
  product?: ICreatedProduct;
  onSubmitHandler: (data: ICreatedProduct) => void;
  onRemoveHandler?: () => void;
  isFetching: boolean;
}

export const CreatedProductForm = ({
  product,
  onSubmitHandler,
  isFetching,
  onRemoveHandler,
}: IProps) => {
  const dispatch = useDispatch();
  const [mode, setMode] = useState<FORM_MODE>();
  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
    register,
    setValue,
  } = useForm({
    defaultValues: {
      title: "",
      description: "",
      price: "",
      isPublished: false,
    },
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    product ? setMode(FORM_MODE.EDIT) : setMode(FORM_MODE.CREATE);
  }, [product]);

  useEffect(() => {
    if (Object.keys(errors).length) {
      const transformErrors = [];
      for (const [key, value] of Object.entries(errors)) {
        transformErrors.push(`${key} - ${value.message} \n`);
      }
      dispatch(
        commonActions.setToast({
          type: TOAST_TYPES.ERROR,
          message: transformErrors?.toString(),
        })
      );
    }
  }, [dispatch, errors]);

  useEffect(() => {
    if (mode === FORM_MODE.EDIT && product) {
      for (const [key, value] of Object.entries(product)) {
        setValue(key as any, value);
      }
    }
  }, [mode, product]);

  const isErrorField = (name: string) => {
    return Object.keys(errors).includes(name)
      ? INPUT_TYPES.ERROR
      : INPUT_TYPES.DEFAULT;
  };

  const onSubmit = (data: ICreatedProduct) => {
    onSubmitHandler(data);
    if (mode === FORM_MODE.CREATE) {
      reset();
    }
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Description
        title="Название"
        content={
          <Controller
            name="title"
            control={control}
            render={({ field }) => (
              <Input
                w={"100%"}
                type={isErrorField(field.name)}
                {...field}
                placeholder={"Название"}
              />
            )}
          />
        }
      />
      <Spacer h={1} />
      <Description
        title="Описание"
        content={
          <Controller
            name="description"
            control={control}
            render={({ field }) => (
              <Input
                w={"100%"}
                type={isErrorField(field.name)}
                {...field}
                placeholder={"Описание"}
              />
            )}
          />
        }
      />
      <Spacer h={1} />
      <Description
        title="Цена"
        content={
          <Controller
            name="price"
            control={control}
            render={({ field }) => (
              <Input
                w={"100%"}
                type={isErrorField(field.name)}
                {...field}
                placeholder={"Цена"}
              />
            )}
          />
        }
      />
      <Spacer h={1} />
      <Description
        title="Опубликован"
        content={
          <>
            <input
              id={"isPublished"}
              type={"checkbox"}
              {...register("isPublished")}
            />
            <label htmlFor={"isPublished"}>Опубликован</label>
          </>
        }
      />
      <Spacer h={1} />
      <Grid.Container justify={"space-between"}>
        <Button
          w={"50%"}
          loading={isFetching}
          htmlType={"submit"}
          type={BUTTON_TYPE.SUCCESS}
        >
          Сохранить
        </Button>
        {mode === FORM_MODE.EDIT && (
          <Button w={"50%"} onClick={onRemoveHandler} type={BUTTON_TYPE.ERROR}>
            Удалить
          </Button>
        )}
      </Grid.Container>
    </form>
  );
};
