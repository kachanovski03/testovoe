import React from "react";
import { Grid, Text } from "@geist-ui/react";
import { CreatedProductForm } from "../createdProductForm";
import { ICreatedProduct } from "../../../../interfaces";

interface IProps {
  createProductHandler: (data: ICreatedProduct) => void;
  isFetching: boolean;
}

export const CreateProduct = ({ createProductHandler, isFetching }: IProps) => {
  return (
    <Grid.Container justify={"center"}>
      <Grid xs={12} direction={"column"}>
        <Text h1>Создать товар</Text>
        <CreatedProductForm
          isFetching={isFetching}
          onSubmitHandler={createProductHandler}
        />
      </Grid>
    </Grid.Container>
  );
};
