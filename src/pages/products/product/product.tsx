import React from "react";
import { IProduct } from "../../../interfaces";
import { Grid, Image, Text } from "@geist-ui/react";

interface IProps {
  product: IProduct;
}

export const Product = ({ product }: IProps) => {
  return (
    <Grid.Container>
      <Grid xs={12}>
        <Image w={20} src={product.image} />
      </Grid>
      <Grid xs={12} direction={"column"}>
        <Text h2>{product.title}</Text>
        <Text h3>{product.price}</Text>
        <Text h4>{product.description}</Text>
      </Grid>
    </Grid.Container>
  );
};
