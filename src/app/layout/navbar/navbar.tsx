import React from "react";
import "./navbar.css";
import { Home, List, LogOut, PlusSquare } from "@geist-ui/react-icons";
import { Grid } from "@geist-ui/react";
import { NavLink } from "react-router-dom";

interface IProps {
  logoutHandler: () => void;
}

export const Navbar = ({ logoutHandler }: IProps) => {
  return (
    <Grid className={"navbar"}>
      <div>
        <NavLink
          className={(props) =>
            `navbar-link ${props.isActive ? "navbar-active" : ""}`
          }
          to="/"
        >
          <Home size={32} />
        </NavLink>
        <NavLink
          className={(props) =>
            `navbar-link ${props.isActive ? "navbar-active" : ""}`
          }
          to={"/products"}
        >
          <List size={32} />
        </NavLink>
        <NavLink
          className={(props) =>
            `navbar-link ${props.isActive ? "navbar-active" : ""}`
          }
          to={"/product/create"}
        >
          <PlusSquare size={32} />
        </NavLink>
      </div>
      <div>
        <NavLink onClick={logoutHandler} className={"navbar-link"} to="/">
          <LogOut size={32} />
        </NavLink>
      </div>
    </Grid>
  );
};
