export interface IProduct {
  category: string;
  description: string;
  id: number;
  image: string;
  price: number;
  rating: IRating;
  title: string;
}

export interface IRating {
  rate: number;
  count: number;
}

export interface ICreatedProduct {
  id: number;
  title: string;
  price: number;
  description: string;
  isPublished: boolean;
  createdAt: Date;
}

export interface ICreatedProductFields {
  title: string;
  price: number;
  description: string;
  isPublished: boolean;
}
