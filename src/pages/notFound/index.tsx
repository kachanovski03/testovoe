import { Card, Grid, Text } from "@geist-ui/react";
import React from "react";
import { Link } from "react-router-dom";

export const NotFound = () => {
  return (
    <Grid.Container
      justify={"center"}
      alignItems={"center"}
      style={{ height: "100vh" }}
    >
      <Card shadow w={60} h={30}>
        <Text h1>Запрашиваемая страница не найдена</Text>
        <Link to={"/"}>
          <Text h3>На главную</Text>
        </Link>
      </Card>
    </Grid.Container>
  );
};
