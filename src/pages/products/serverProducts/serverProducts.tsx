import { Grid, Input, Select, Spacer } from "@geist-ui/react";
import React, { SetStateAction } from "react";
import { IProduct } from "../../../interfaces";
import { Card } from "../../../components/card/card";
import { SORT_TYPE } from "../../../constants";
import { Search } from "@geist-ui/react-icons";

interface IProps {
  products: IProduct[];
  searchValue: string;
  removeItem: (id: number) => void;
  onPressCard: (id: number) => void;
  editItemHandler: (id: number) => void;
  getProductsWithLimit: (value: string | string[]) => void;
  getProductsWithSort: (value: string | string[]) => void;
  setSearchValue: React.Dispatch<SetStateAction<string>>;
}

export const ServerProducts = ({
  products,
  removeItem,
  editItemHandler,
  getProductsWithLimit,
  getProductsWithSort,
  searchValue,
  onPressCard,
  setSearchValue,
}: IProps) => {
  return (
    <Grid.Container direction={"column"}>
      <Grid xs={24} justify={"space-between"}>
        <Select
          placeholder="Количество товаров"
          onChange={getProductsWithLimit}
        >
          <Select.Option value="8">8</Select.Option>
          <Select.Option value="16">16</Select.Option>
          <Select.Option value="20">20</Select.Option>
        </Select>
        <Select placeholder="Сортировать" onChange={getProductsWithSort}>
          <Select.Option value={SORT_TYPE.ASC}>А-Я</Select.Option>
          <Select.Option value={SORT_TYPE.DESC}>Я-А</Select.Option>
        </Select>
      </Grid>
      <Spacer h={1} />
      <Grid>
        <Input
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
          iconRight={<Search />}
          placeholder="Поиск"
        />
      </Grid>
      <Spacer h={1} />
      <Grid.Container>
        {products
          .filter((p) =>
            p.title.toLowerCase().includes(searchValue.toLowerCase())
          )
          .map((i) => (
            <Grid xs={6} sm={8} key={i.id}>
              <Card
                onPressCard={onPressCard}
                editHandler={editItemHandler}
                removeHandler={removeItem}
                item={i}
              />
            </Grid>
          ))}
      </Grid.Container>
    </Grid.Container>
  );
};
