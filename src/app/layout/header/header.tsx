import React from "react";
import "./header.css";

export const Header = () => {
  return (
    <div
      className={"header"}
      style={{ paddingRight: 20, height: "8vh", paddingLeft: 20 }}
    />
  );
};
