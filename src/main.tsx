import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import {
  CreateProductPage,
  EditCreatedProductPage,
  EditServerProductPage,
  LoginPage,
  MainPage,
  NotFoundPage,
  ProductPage,
  ProductsPage,
} from "./pages";
import { useDispatch, useSelector } from "react-redux";
import { RootStateType } from "./redux/store";
import { authMe } from "./redux/thunks";
import { Loader } from "./components";
import { ICommonReducer } from "./redux/reducers";
import { Layout } from "./app";

export const Main = () => {
  const dispatch = useDispatch();
  const { isInitialized } = useSelector<RootStateType, ICommonReducer>(
    (state) => state.common
  );

  useEffect(() => {
    dispatch(authMe());
  }, [dispatch]);

  if (!isInitialized) {
    return <Loader />;
  }

  return (
    <Routes>
      <Route path={"/login"} element={<LoginPage />} />
      <Route path={"/"} element={<Layout />}>
        <Route path={""} element={<MainPage />} />
        <Route path={"products"} element={<ProductsPage />} />
        <Route path={"product/:id"} element={<ProductPage />} />
        <Route path={"product/create"} element={<CreateProductPage />} />
        <Route path={"product/edit/:id"} element={<EditServerProductPage />} />
        <Route
          path={"product/created/edit/:id"}
          element={<EditCreatedProductPage />}
        />
      </Route>
      <Route path="*" element={<NotFoundPage />} />
    </Routes>
  );
};
