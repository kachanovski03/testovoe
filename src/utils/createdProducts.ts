import { ICreatedProduct } from "../interfaces";
import { CREATED_PRODUCT_FILTER_TYPE } from "../constants";

export const filterCreatedProducts = (
  products: ICreatedProduct[],
  filter: CREATED_PRODUCT_FILTER_TYPE
) => {
  switch (filter) {
    case CREATED_PRODUCT_FILTER_TYPE.ALL:
      return products;
    case CREATED_PRODUCT_FILTER_TYPE.PUBLISHED:
      return products.filter((i) => i.isPublished);
    case CREATED_PRODUCT_FILTER_TYPE.UNPUBLISHED:
      return products.filter((i) => !i.isPublished);
    default:
      return products;
  }
};
