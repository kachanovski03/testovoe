import React, { useState } from "react";
import { Button, Modal } from "@geist-ui/react";
import { BUTTON_TYPE } from "../../constants";

interface IProps {
  isFetching?: boolean;
  buttonType: BUTTON_TYPE;
  onPressConfirm: () => void;
  cancelButtonTitle?: string;
  confirmButtonTitle?: string;
  confirmTitle?: string;
  icon?: React.ReactNode;
  scale?: number
}

export const ButtonWithConfirm = ({
  isFetching,
  onPressConfirm,
  icon,
  buttonType = BUTTON_TYPE.ERROR,
  confirmTitle = "Удалить?",
  confirmButtonTitle = "Удалить",
  cancelButtonTitle = "Отмена",
                                    scale = 1/3
}: IProps) => {
  const [open, setOpen] = useState(false);
  const openModal = () => setOpen(true);

  const closeModal = () => {
    setOpen(false);
  };

  const onPressConfirmHandler = () => {
    onPressConfirm();
    setOpen(false);
  };

  return (
    <>
      <Button
        onClick={openModal}
        scale={scale}
        type={buttonType}
        iconRight={icon}
        auto
      />
      <Modal visible={open} onClose={closeModal}>
        <Modal.Title>{confirmTitle}</Modal.Title>
        <Modal.Action passive onClick={closeModal}>
          {cancelButtonTitle}
        </Modal.Action>
        <Modal.Action loading={isFetching} onClick={onPressConfirmHandler}>
          {confirmButtonTitle}
        </Modal.Action>
      </Modal>
    </>
  );
};
