import { InferActionsType } from "../store";
import { commonActions } from "../actions";
import {
  SET_IS_FETCHING,
  SET_IS_INITIALIZED,
  SET_STATUS,
  SET_TOAST,
} from "../constants";
import { IToastData } from "../../interfaces";

export type IActions = InferActionsType<typeof commonActions>;
export type ICommonReducer = typeof defaultState;

const defaultState = {
  isFetching: false,
  isInitialized: false as boolean,
  status: "",
  toastData: {} as IToastData,
};

export const commonReducer = (
  state = defaultState,
  action: IActions
): ICommonReducer => {
  switch (action.type) {
    case SET_TOAST:
    case SET_IS_FETCHING:
    case SET_IS_INITIALIZED:
    case SET_STATUS:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
