import React from "react";
import { Display, Grid, Spacer } from "@geist-ui/react";
import { Gitlab } from "@geist-ui/react-icons";
import { LoginForm } from "./loginForm";
import { IAuthForm } from "../../../interfaces";

interface IProps {
  isFetching: boolean;
  handleLogin: (data: IAuthForm) => void;
}

export const LoginPage = ({ handleLogin, isFetching }: IProps) => {
  return (
    <Grid.Container
      justify="center"
      alignItems={"center"}
      style={{ height: "100vh" }}
    >
      <Display
        w={"500px"}
        shadow
        caption="An open-source design system for building modern websites and applications."
      >
        <Grid.Container justify={"center"} alignItems={"center"}>
          <Grid>
            <Spacer h={2} />
            <Gitlab size={45} />
          </Grid>
          <Grid direction={"column"} xs={22}>
            <LoginForm onSubmit={handleLogin} isFetching={isFetching} />
          </Grid>
        </Grid.Container>
      </Display>
    </Grid.Container>
  );
};
