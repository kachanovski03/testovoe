import React from "react";
import { Form } from "../../../components";
import { Button, Input, Spacer } from "@geist-ui/react";
import * as yup from "yup";
import { IAuthForm } from "../../../interfaces";
import { BUTTON_TYPE } from "../../../constants";

const schema = yup.object().shape({
  username: yup.string().required("Это поле обязательно для заполнения"),
  password: yup.string().required("Это поле обязательно для заполнения"),
});

interface IProps {
  onSubmit: (data: IAuthForm) => void;
  isFetching: boolean;
}

export const LoginForm = ({ onSubmit, isFetching }: IProps) => {
  return (
    <Form
      schema={schema}
      defaultValues={{ username: "mor_2314", password: "83r5^_" }}
      onSubmit={onSubmit}
    >
      <Spacer h={3} />
      <Input name={"username"} width={"100%"} placeholder="Логин" />
      <Spacer h={1} />
      <Input.Password name={"password"} width={"100%"} placeholder="Пароль" />
      <Spacer h={1} />
      <Button
        loading={isFetching}
        htmlType={"submit"}
        width="100%"
        type={BUTTON_TYPE.SUCCESS}
      >
        Login
      </Button>
      <Spacer h={3} />
    </Form>
  );
};
