import { IProduct } from "../../interfaces";
import { SET_PRODUCT, SET_PRODUCTS } from "../constants";

export const productsActions = {
  setProducts: (products: IProduct[]) => ({
    type: SET_PRODUCTS,
    payload: { products },
  }),
  setProduct: (product: IProduct) => ({
    type: SET_PRODUCT,
    payload: { product },
  }),
};
