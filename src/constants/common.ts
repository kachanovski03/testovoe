export enum SORT_TYPE {
  DESC = "desc",
  ASC = "asc",
}

export enum BUTTON_TYPE {
  ERROR = "error",
  SECONDARY = "secondary",
  SUCCESS = "success",
}

export enum TOAST_TYPES {
  ERROR = "error",
  SECONDARY = "secondary",
  DEFAULT = "default",
  SUCCESS = "success",
  WARNING = "warning",
}

export enum LOCALSTORAGE_KEYS {
  CREATED_PRODUCTS = "createdProduct",
  TOKEN = "token",
}

export enum FORM_MODE {
  CREATE,
  EDIT,
}

export enum INPUT_TYPES {
  ERROR = "error",
  DEFAULT = "default",
}
