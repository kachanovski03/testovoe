import { productsReducer, IProductReducer } from "./products";
import { commonReducer, ICommonReducer } from "./common";
import { authReducer, IAuthReducer } from "./auth";

export { productsReducer, commonReducer, authReducer };

export type { IProductReducer, ICommonReducer, IAuthReducer };
