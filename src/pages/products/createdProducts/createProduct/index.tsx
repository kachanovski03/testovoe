import React from "react";
import { CreateProduct } from "./createProduct";
import { useDispatch, useSelector } from "react-redux";
import { createProduct } from "../../../../redux/thunks";
import { RootStateType } from "../../../../redux/store";
import { ICommonReducer } from "../../../../redux/reducers";
import { ICreatedProduct } from "../../../../interfaces";

export const Container = () => {
  const dispatch = useDispatch();
  const { isFetching } = useSelector<RootStateType, ICommonReducer>(
    (state) => state.common
  );

  const createProductHandler = (data: ICreatedProduct) => {
    dispatch(createProduct(data));
  };

  return (
    <CreateProduct
      isFetching={isFetching}
      createProductHandler={createProductHandler}
    />
  );
};
