import { commonThunkCreator } from "../utils";
import {
  commonActions,
  createdProductsActions,
  productsActions,
} from "../actions";
import { BaseThunkType, InferActionsType } from "../store";
import { productAPI } from "../../api";
import { ICreatedProduct, ICreatedProductFields } from "../../interfaces";
import { localStorageUtils } from "../../utils";
import { LOCALSTORAGE_KEYS, TOAST_TYPES } from "src/constants";

type ThunkType = BaseThunkType<
  InferActionsType<
    | typeof createdProductsActions
    | typeof commonActions
    | typeof productsActions
  >
>;

export const createProduct =
  (product: ICreatedProductFields): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      await productAPI.createProduct(product);
      const newProduct: ICreatedProduct = {
        ...product,
        id: Date.now(),
        createdAt: new Date(),
      };
      localStorageUtils.saveItems(
        LOCALSTORAGE_KEYS.CREATED_PRODUCTS,
        newProduct
      );
      dispatch(
        commonActions.setToast({
          type: TOAST_TYPES.SUCCESS,
          message: "Товар успешно создан",
        })
      );
    }, dispatch);
  };

export const getCreatedProductById =
  (key: string, id: number): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      const products: ICreatedProduct[] = localStorageUtils.getItem(key);
      const product = products.find((i) => i.id === id);
      product
        ? dispatch(createdProductsActions.setCreatedProduct(product))
        : dispatch(
            commonActions.setToast({
              type: TOAST_TYPES.ERROR,
              message: "Такого товара не найдено",
            })
          );
    }, dispatch);
  };

export const getCreatedProducts =
  (key: string): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      const res: ICreatedProduct[] = localStorageUtils.getItem(key) ?? [];
      dispatch(createdProductsActions.setCreatedProducts(res));
    }, dispatch);
  };

export const removeCreatedProduct =
  (key: string, id: number): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      const oldProducts = localStorageUtils.getItem(key);
      const products = oldProducts.filter((i: ICreatedProduct) => i.id !== id);
      localStorageUtils.saveItem(key, products);
      dispatch(createdProductsActions.setCreatedProducts(products));
      dispatch(
        commonActions.setToast({
          type: TOAST_TYPES.SUCCESS,
          message: "Товар успешно удален",
        })
      );
    }, dispatch);
  };

export const editCreatedProduct =
  (key: string, id: number, product: ICreatedProduct): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      const oldProducts: ICreatedProduct[] = localStorageUtils.getItem(key);
      const products = oldProducts.map((i) =>
        i.id === id ? { ...i, ...product } : i
      );
      localStorageUtils.saveItem(key, products);
      dispatch(createdProductsActions.setCreatedProducts(products));
      dispatch(
        commonActions.setToast({
          type: TOAST_TYPES.SUCCESS,
          message: "Товар успешно изменен",
        })
      );
    }, dispatch);
  };
