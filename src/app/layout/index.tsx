import React, { useEffect } from "react";
import { Navigate, Outlet } from "react-router-dom";
import { LayoutComponent } from "./layout";
import { useDispatch, useSelector } from "react-redux";
import { RootStateType } from "../../redux/store";
import { ICommonReducer } from "../../redux/reducers";
import { logout } from "../../redux/thunks";
import { useToasts } from "@geist-ui/react";

export const Container = () => {
  const dispatch = useDispatch();
  const [, setToast] = useToasts();
  const isAuth = useSelector<RootStateType, boolean>(
    (state) => state.auth.isAuth
  );
  const { isFetching, toastData } = useSelector<RootStateType, ICommonReducer>(
    (state) => state.common
  );

  useEffect(() => {
    if (toastData.message && toastData.type)
      setToast({
        text: toastData.message,
        type: toastData.type,
      });
  }, [toastData]);

  const logoutHandler = () => {
    dispatch(logout());
  };

  if (!isAuth) {
    return <Navigate to="/login" />;
  }
  return (
    <LayoutComponent logoutHandler={logoutHandler} isFetching={isFetching}>
      <Outlet />
    </LayoutComponent>
  );
};
