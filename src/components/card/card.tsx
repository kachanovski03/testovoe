import React from "react";
import "./card.css";
import {Button, ButtonGroup, Card as CardComponent, Image, Text,} from "@geist-ui/react";
import {IProduct} from "../../interfaces";
import {Delete, Edit2} from "@geist-ui/react-icons";
import {ButtonWithConfirm} from "../index";
import {BUTTON_TYPE} from "../../constants";

interface IProps {
    item: IProduct;
    onPressCard: (id: number) => void;
    editHandler: (id: number) => void;
    removeHandler: (id: number) => void;
}

export const Card = ({
                         item,
                         editHandler,
                         removeHandler,
                         onPressCard,
                     }: IProps) => {
    const onPressRemove = () => {
        removeHandler(item.id);
    };
    const onPressEdit = () => {
        editHandler(item.id);
    };
    const onPressCardHandler = () => {
        onPressCard(item.id);
    };
    return (
        <CardComponent
            onClick={onPressCardHandler}
            className={"card"}
            shadow
            width="100%"
        >
            <Image
                w={"250px"}
                h={"250px"}
                className={"card-image"}
                src={item.image}
            />
            <Text className={"card-title"}>{item.title}</Text>
            <div className={"card-actions"}>
                <Text h5>{`${item.price}$`}</Text>
                <ButtonGroup onClick={(e) => e.stopPropagation()}>
                    <Button onClick={onPressEdit} iconRight={<Edit2/>} auto/>
                    <ButtonWithConfirm scale={1} buttonType={BUTTON_TYPE.SECONDARY} icon={<Delete/>}
                                       onPressConfirm={onPressRemove}/>
                </ButtonGroup>
            </div>
        </CardComponent>
    );
};
