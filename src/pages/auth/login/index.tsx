import React, { useEffect, useState } from "react";
import { LoginPage } from "./loginPage";
import { useDispatch, useSelector } from "react-redux";
import { login } from "../../../redux/thunks";
import { useLocation, useNavigate } from "react-router-dom";
import { RootStateType } from "../../../redux/store";
import { useToasts } from "@geist-ui/react";
import { IAuthForm, IToastData } from "../../../interfaces";

export const Container = () => {
  const dispatch = useDispatch();
  const [, setToast] = useToasts();
  const navigate = useNavigate();
  const location = useLocation();

  const from = location.state?.from?.pathname || "/";

  const [isFetching, setIsFetching] = useState<boolean>(false);
  const isAuth = useSelector<RootStateType, boolean>(
    (state) => state.auth.isAuth
  );
  const toastData = useSelector<RootStateType, IToastData>(
    (state) => state.common.toastData
  );

  useEffect(() => {
    if (toastData.message && toastData.type)
      setToast({
        text: toastData.message,
        type: toastData.type,
      });
  }, [toastData]);

  useEffect(() => {
    if (isAuth) {
      navigate(from, { replace: true });
    }
  }, [from, isAuth]);

  const handleLogin = async (data: IAuthForm) => {
    setIsFetching(true);
    await dispatch(login(data));
    setIsFetching(false);
  };

  return <LoginPage isFetching={isFetching} handleLogin={handleLogin} />;
};
