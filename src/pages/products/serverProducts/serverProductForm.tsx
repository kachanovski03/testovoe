import React, { useEffect } from "react";
import { commonActions } from "../../../redux/actions";
import { useDispatch } from "react-redux";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { Button, Description, Grid, Input, Spacer } from "@geist-ui/react";
import { BUTTON_TYPE, INPUT_TYPES, TOAST_TYPES } from "../../../constants";
import { IProduct } from "../../../interfaces";

const schema = yup.object().shape({
  title: yup.string().required("Это поле обязательно для заполнения"),
  description: yup.string().required("Это поле обязательно для заполнения"),
  price: yup.number().required("Это поле обязательно для заполнения"),
});

interface IProps {
  product: IProduct;
  editProductHandler: (data: IProduct) => void;
  isFetching: boolean;
  onRemoveHandler?: () => void;
}

export const ServerProductForm = ({
  product,
  onRemoveHandler,
  editProductHandler,
  isFetching,
}: IProps) => {
  const dispatch = useDispatch();
  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    defaultValues: {
      title: "",
      description: "",
      price: "",
    },
    resolver: yupResolver(schema),
  });

  useEffect(() => {
    if (Object.keys(errors).length) {
      const transformErrors = [];
      for (const [key, value] of Object.entries(errors)) {
        transformErrors.push(`${key} - ${value.message} \n`);
      }
      dispatch(
        commonActions.setToast({
          type: TOAST_TYPES.ERROR,
          message: transformErrors?.toString(),
        })
      );
    }
  }, [dispatch, errors]);

  useEffect(() => {
    for (const [key, value] of Object.entries(product)) {
      setValue(key as any, value);
    }
  }, [product]);

  const isErrorField = (name: string) => {
    return Object.keys(errors).includes(name)
      ? INPUT_TYPES.ERROR
      : INPUT_TYPES.DEFAULT;
  };

  const onSubmit = (data: IProduct) => {
    editProductHandler(data);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Description
        title="Название"
        content={
          <Controller
            name="title"
            control={control}
            render={({ field }) => (
              <Input
                w={"100%"}
                type={isErrorField(field.name)}
                {...field}
                placeholder={"Название"}
              />
            )}
          />
        }
      />
      <Spacer h={1} />
      <Description
        title="Описание"
        content={
          <Controller
            name="description"
            control={control}
            render={({ field }) => (
              <Input
                w={"100%"}
                type={isErrorField(field.name)}
                {...field}
                placeholder={"Описание"}
              />
            )}
          />
        }
      />
      <Spacer h={1} />
      <Description
        title="Цена"
        content={
          <Controller
            name="price"
            control={control}
            render={({ field }) => (
              <Input
                w={"100%"}
                type={isErrorField(field.name)}
                {...field}
                placeholder={"Цена"}
              />
            )}
          />
        }
      />
      <Spacer h={1} />
      <Grid.Container justify={"space-between"}>
        <Button
          w={"50%"}
          loading={isFetching}
          htmlType={"submit"}
          type={BUTTON_TYPE.SUCCESS}
        >
          Сохранить
        </Button>
        <Button w={"50%"} onClick={onRemoveHandler} type={BUTTON_TYPE.ERROR}>
          Удалить
        </Button>
      </Grid.Container>
    </form>
  );
};
