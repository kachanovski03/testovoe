import React, { useEffect } from "react";
import { Product } from "./product";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { getProductById } from "../../../redux/thunks";
import { RootStateType } from "../../../redux/store";
import { IProduct } from "../../../interfaces";
import { productsActions } from "../../../redux/actions";

export const Container = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const product = useSelector<RootStateType, IProduct>(
    (state) => state.products.product
  );
  useEffect(() => {
    dispatch(getProductById(Number(id)));
    return () => {
      dispatch(productsActions.setProduct({} as IProduct));
    };
  }, [dispatch, id]);

  return <Product product={product} />;
};
