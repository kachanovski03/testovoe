import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  getCreatedProducts,
  removeCreatedProduct,
} from "../../../redux/thunks";
import { RootStateType } from "../../../redux/store";
import { ICreatedProduct } from "../../../interfaces";
import { CreatedProducts } from "./createdProducts";
import {
  CREATED_PRODUCT_FILTER_TYPE,
  LOCALSTORAGE_KEYS,
} from "../../../constants";
import { Loading } from "@geist-ui/react";

export const Container = () => {
  const dispatch = useDispatch();
  const navigation = useNavigate();

  const [filterType, setFilterType] = useState<CREATED_PRODUCT_FILTER_TYPE>(
    CREATED_PRODUCT_FILTER_TYPE.ALL
  );

  const createdProducts = useSelector<RootStateType, ICreatedProduct[]>(
    (state) => state.createdProducts.createdProducts
  );
  const isFetching = useSelector<RootStateType, boolean>(
    (state) => state.common.isFetching
  );

  useEffect(() => {
    dispatch(getCreatedProducts(LOCALSTORAGE_KEYS.CREATED_PRODUCTS));
  }, [dispatch]);

  const removeCreatedProductHandler = (id: number) => {
    dispatch(removeCreatedProduct(LOCALSTORAGE_KEYS.CREATED_PRODUCTS, id));
  };

  const editCreatedProduct = (id: number) => {
    navigation(`/product/created/edit/${id}`);
  };
  const changeFilterType = (type: string | string[]) => {
    setFilterType(type as CREATED_PRODUCT_FILTER_TYPE);
  };

  if (isFetching) {
    return <Loading />;
  }

  return (
    <CreatedProducts
      filterType={filterType}
      changeFilterType={changeFilterType}
      editCreatedProduct={editCreatedProduct}
      removeCreatedProduct={removeCreatedProductHandler}
      createdProducts={createdProducts}
    />
  );
};
