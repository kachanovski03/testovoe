import { BaseThunkType, InferActionsType } from "../store";
import { authActions, commonActions } from "../actions";
import { commonThunkCreator } from "../utils";
import { authAPI } from "../../api";
import { IAuthForm } from "../../interfaces";
import { localStorageUtils } from "../../utils";
import { LOCALSTORAGE_KEYS, TOAST_TYPES } from "../../constants";

type ThunkType = BaseThunkType<
  InferActionsType<typeof authActions | typeof commonActions>
>;

export const login =
  (data: IAuthForm): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      const res = await authAPI.login(data);
      if (res.data.status === "Error") {
        dispatch(commonActions.setStatus("Error"));
        dispatch(
          commonActions.setToast({
            type: TOAST_TYPES.ERROR,
            message: res.data.msg,
          })
        );
      } else {
        localStorageUtils.saveItem(LOCALSTORAGE_KEYS.TOKEN, res.data.token);
        await dispatch(authMe());
      }
    }, dispatch);
  };

export const logout = (): ThunkType => async (dispatch) => {
  await localStorageUtils.removeItem(LOCALSTORAGE_KEYS.TOKEN);
  await dispatch(authMe());
};

export const authMe = (): ThunkType => async (dispatch) => {
  const res = await localStorageUtils.getItem(LOCALSTORAGE_KEYS.TOKEN);
  res
    ? dispatch(authActions.setIsAuth(true))
    : dispatch(authActions.setIsAuth(false));
  dispatch(commonActions.setIsInitialize(true));
};
