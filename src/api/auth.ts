import { instance } from "./instance";
import { IAuthForm } from "../interfaces";

export const authAPI = {
  login(data: IAuthForm) {
    return instance.post("auth/login", data);
  },
};
