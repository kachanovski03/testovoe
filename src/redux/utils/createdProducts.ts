import { ICreatedProduct } from "../../interfaces";

export const filterCreatedProducts = (
  products: ICreatedProduct[],
  filter: string
) => {
  switch (filter) {
    case "all":
      return products;
    case "published":
      return products.filter((i) => i.isPublished);
    case "unpublished":
      return products.filter((i) => !i.isPublished);
    default:
      return products;
  }
};
