import React from "react";
import { Tabs, useTabs } from "@geist-ui/react";
import { FILTER_TYPE_PRODUCTS_PAGE } from "../../constants";
import { Container as ServerProducts } from "./serverProducts";
import { Container as CreatedProducts } from "./createdProducts";

export const Container = () => {
  const { bindings } = useTabs(FILTER_TYPE_PRODUCTS_PAGE.SERVER);
  return (
    <Tabs {...bindings}>
      <Tabs.Item
        label={FILTER_TYPE_PRODUCTS_PAGE.SERVER}
        value={FILTER_TYPE_PRODUCTS_PAGE.SERVER}
      >
        <ServerProducts />
      </Tabs.Item>
      <Tabs.Item
        label={FILTER_TYPE_PRODUCTS_PAGE.CREATED}
        value={FILTER_TYPE_PRODUCTS_PAGE.CREATED}
      >
        <CreatedProducts />
      </Tabs.Item>
    </Tabs>
  );
};
