import React, { useEffect } from "react";
import { Controller, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useToasts } from "@geist-ui/react";
import { TOAST_TYPES } from "../../constants";

interface IProps {
  defaultValues?: { [x: string]: any } | undefined;
  children?: React.ReactNode;
  onSubmit: (data: any) => void;
  schema: any;
}

interface EnrichedChildren {
  name: string;
}

export const Form = ({ defaultValues, children, onSubmit, schema }: IProps) => {
  const methods = useForm({ defaultValues, resolver: yupResolver(schema) });
  const {
    handleSubmit,
    formState: { errors },
    control,
  } = methods;
  const [, setToast] = useToasts();
  useEffect(() => {
    if (Object.keys(errors)[0]) {
      setToast({
        text: Object.values(errors)[0].message,
        type: TOAST_TYPES.ERROR,
      });
    }
  }, [errors]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {React.Children.map(children, (child) => {
        if (!React.isValidElement<EnrichedChildren>(child)) {
          return child;
        }
        return child.props.name ? (
          <Controller
            name={child.props.name}
            control={control}
            render={({ field, formState: { errors } }) =>
              React.createElement(child.type, {
                ...{
                  ...field,
                  ...child.props,
                  type: errors.hasOwnProperty(child.props.name) ? "error" : "",
                  key: child.props.name,
                },
              })
            }
          />
        ) : (
          child
        );
      })}
    </form>
  );
};
