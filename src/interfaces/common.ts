import { TOAST_TYPES } from "../constants";

export interface IToastData {
  type: TOAST_TYPES;
  message: string;
}
