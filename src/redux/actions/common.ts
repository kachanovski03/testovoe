import {
  SET_IS_FETCHING,
  SET_IS_INITIALIZED,
  SET_STATUS,
  SET_TOAST,
} from "../constants";
import { IToastData } from "../../interfaces";

export const commonActions = {
  setIsFetching: (isFetching: boolean) =>
    ({ type: SET_IS_FETCHING, payload: { isFetching } } as const),
  setStatus: (status: string) =>
    ({ type: SET_STATUS, payload: { status } } as const),
  setToast: (toastData: IToastData) =>
    ({ type: SET_TOAST, payload: { toastData } } as const),
  setIsInitialize: (isInitialized: boolean) =>
    ({ type: SET_IS_INITIALIZED, payload: { isInitialized } } as const),
};
