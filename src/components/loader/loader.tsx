import React from "react";
import { Loading } from "@geist-ui/react";

export const Loader = () => {
  return <Loading style={{ height: "92vh" }}>Loading</Loading>;
};
