import React, { useEffect } from "react";
import { EditServerProduct } from "./editServerProduct";
import { useNavigate, useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { RootStateType } from "../../../../redux/store";
import { IProduct } from "../../../../interfaces";
import {
  getProductById,
  removeProduct,
  updateProduct,
} from "../../../../redux/thunks";
import { ICommonReducer } from "../../../../redux/reducers";
import { productsActions } from "../../../../redux/actions";

export const Container = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const product = useSelector<RootStateType, IProduct>(
    (state) => state.products.product
  );
  const { isFetching } = useSelector<RootStateType, ICommonReducer>(
    (state) => state.common
  );
  useEffect(() => {
    dispatch(getProductById(Number(id)));
    return () => {
      dispatch(productsActions.setProduct({} as IProduct));
    };
  }, [dispatch, id]);

  const editProductHandler = (data: IProduct) => {
    dispatch(updateProduct(Number(id), data));
  };
  const onRemoveHandler = () => {
    dispatch(removeProduct(Number(id)));
    navigate("/products");
  };

  return (
    <EditServerProduct
      onRemoveHandler={onRemoveHandler}
      isFetching={isFetching}
      editProductHandler={editProductHandler}
      product={product}
    />
  );
};
