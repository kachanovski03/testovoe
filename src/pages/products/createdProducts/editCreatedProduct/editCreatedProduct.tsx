import React from "react";
import { ICreatedProduct } from "../../../../interfaces";
import { Grid, Text } from "@geist-ui/react";
import { CreatedProductForm } from "../createdProductForm";

interface IProps {
  product: ICreatedProduct;
  editProductHandler: (data: ICreatedProduct) => void;
  isFetching: boolean;
  removeCreatedProductHandler?: () => void;
}

export const EditCreatedProduct = ({
  product,
  removeCreatedProductHandler,
  editProductHandler,
  isFetching,
}: IProps) => {
  return (
    <Grid.Container justify={"center"}>
      <Grid xs={12} direction={"column"}>
        <Text h1>Редактировать созданный товар</Text>
        <CreatedProductForm
          onRemoveHandler={removeCreatedProductHandler}
          product={product}
          onSubmitHandler={editProductHandler}
          isFetching={isFetching}
        />
      </Grid>
    </Grid.Container>
  );
};
