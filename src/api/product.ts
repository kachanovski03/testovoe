import { instance } from "./instance";
import { ICreatedProductFields, IProduct } from "../interfaces";
import { SORT_TYPE } from "../constants";

export const productAPI = {
  getProducts(limit: number = 8, sort: SORT_TYPE = SORT_TYPE.ASC) {
    return instance.get("products", { params: { limit, sort } });
  },
  getProductById(id: number) {
    return instance.get(`products/${id}`);
  },
  createProduct(product: ICreatedProductFields) {
    return instance.post(`products`, product);
  },
  updateProduct(id: number, product: IProduct) {
    return instance.put(`products/${id}`, product);
  },
  removeProduct(id: number) {
    return instance.delete(`products/${id}`);
  },
};
