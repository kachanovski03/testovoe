import { Container as LoginPage } from "./auth/login";
import { Container as ProductsPage } from "./products";
import { Container as ProductPage } from "./products/product";
import { Container as CreateProductPage } from "./products/createdProducts/createProduct";
import { Container as EditServerProductPage } from "./products/serverProducts/editServerProduct";
import { Container as EditCreatedProductPage } from "./products/createdProducts/editCreatedProduct";
import { NotFound as NotFoundPage } from "./notFound";
import { MainPage } from "./main/mainPage";

export {
  LoginPage,
  MainPage,
  NotFoundPage,
  ProductsPage,
  ProductPage,
  CreateProductPage,
  EditServerProductPage,
  EditCreatedProductPage,
};
