import { commonThunkCreator } from "../utils";
import { commonActions, productsActions } from "../actions";
import { BaseThunkType, InferActionsType } from "../store";
import { productAPI } from "../../api";
import { IProduct } from "../../interfaces";
import { SORT_TYPE, TOAST_TYPES } from "../../constants";

type ThunkType = BaseThunkType<
  InferActionsType<typeof productsActions | typeof commonActions>
>;

export const getProducts =
  (limit?: number, sort?: SORT_TYPE): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      const res = await productAPI.getProducts(limit, sort);
      dispatch(productsActions.setProducts(res.data));
    }, dispatch);
  };
export const getProductById =
  (id: number): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      const res = await productAPI.getProductById(id);
      res.data
        ? dispatch(productsActions.setProduct(res.data))
        : dispatch(
            commonActions.setToast({
              type: TOAST_TYPES.ERROR,
              message: "Такого товара не найдено",
            })
          );
    }, dispatch);
  };

export const removeProduct =
  (id: number): ThunkType =>
  async (dispatch, getState) => {
    await commonThunkCreator(async () => {
      const res = await productAPI.removeProduct(id);
      const store = getState();
      const filteredProducts = store.products.products.filter(
        (i) => i.id !== res.data.id
      );
      dispatch(productsActions.setProducts(filteredProducts));
      dispatch(
        commonActions.setToast({
          type: TOAST_TYPES.SUCCESS,
          message: "Товар успешно удален",
        })
      );
    }, dispatch);
  };

export const updateProduct =
  (id: number, product: IProduct): ThunkType =>
  async (dispatch) => {
    await commonThunkCreator(async () => {
      await productAPI.updateProduct(id, product);
      dispatch(
        commonActions.setToast({
          type: TOAST_TYPES.SUCCESS,
          message: "Товар успешно изменен",
        })
      );
    }, dispatch);
  };
