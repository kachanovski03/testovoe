import React from "react";
import "./layout.css";
import { Header } from "./header/header";
import { Navbar } from "./navbar/navbar";

interface IProps {
  children: React.ReactNode;
  isFetching: boolean;
  logoutHandler: () => void;
}

export const LayoutComponent = ({ children, logoutHandler }: IProps) => {
  return (
    <div className={"layout"}>
      <Navbar logoutHandler={logoutHandler} />
      <Header />
      <div className={"content"}>{children}</div>
    </div>
  );
};
