import React from "react";
import { ICreatedProduct } from "../../../interfaces";
import { Button, Grid, Select, Spacer, Table, Text } from "@geist-ui/react";
import { Edit2, Delete } from "@geist-ui/react-icons";
import { ButtonWithConfirm } from "../../../components";
import { BUTTON_TYPE, CREATED_PRODUCT_FILTER_TYPE } from "../../../constants";
import { filterCreatedProducts } from "../../../utils/createdProducts";

interface IProps {
  filterType: CREATED_PRODUCT_FILTER_TYPE;
  createdProducts: ICreatedProduct[];
  removeCreatedProduct: (id: number) => void;
  editCreatedProduct: (id: number) => void;
  changeFilterType: (value: string | string[]) => void;
}

export const CreatedProducts = ({
  filterType,
  createdProducts,
  removeCreatedProduct,
  editCreatedProduct,
  changeFilterType,
}: IProps) => {
  if (!createdProducts.length) {
    return <Text h2>Здесь пока нет никаких записей</Text>;
  }
  const renderActions = (id: number) => {
    return (
      <>
        <Button
          onClick={() => editCreatedProduct(id)}
          scale={1 / 3}
          type={BUTTON_TYPE.SECONDARY}
          iconRight={<Edit2 />}
          auto
        />
        <ButtonWithConfirm
          onPressConfirm={() => removeCreatedProduct(id)}
          buttonType={BUTTON_TYPE.ERROR}
          icon={<Delete />}
        />
      </>
    );
  };

  return (
    <>
      <Grid.Container direction={"column"}>
        <Grid>
          <Select placeholder="Тип" onChange={changeFilterType}>
            <Select.Option value={CREATED_PRODUCT_FILTER_TYPE.ALL}>
              Все
            </Select.Option>
            <Select.Option value={CREATED_PRODUCT_FILTER_TYPE.PUBLISHED}>
              Опубликованные
            </Select.Option>
            <Select.Option value={CREATED_PRODUCT_FILTER_TYPE.UNPUBLISHED}>
              Неопубликованные
            </Select.Option>
          </Select>
        </Grid>
        <Spacer h={1} />
        <Grid>
          <Table data={filterCreatedProducts(createdProducts, filterType)}>
            <Table.Column prop="title" label="Название" />
            <Table.Column prop="description" label="Описание" />
            <Table.Column prop="price" label="Цена" />
            <Table.Column
              prop="isPublished"
              render={(rowData) => rowData?.toString()}
              label="Опубликован"
            />
            <Table.Column
              prop="id"
              render={(rowData) => renderActions(rowData)}
            />
          </Table>
        </Grid>
      </Grid.Container>
    </>
  );
};
