export enum CREATED_PRODUCT_FILTER_TYPE {
  ALL = "all",
  PUBLISHED = "published",
  UNPUBLISHED = "unpublished",
}

export enum FILTER_TYPE_PRODUCTS_PAGE {
  CREATED = "Созданные продукты",
  SERVER = "Продукты",
}
