export const SET_IS_FETCHING = "SET_IS_FETCHING";
export const SET_STATUS = "SET_STATUS";
export const SET_TOAST = "SET_TOAST";
export const SET_IS_INITIALIZED = "SET_IS_INITIALIZED";
